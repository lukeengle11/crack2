#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a hashed guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    int retValue = 0;
    // Compare the two hashes
    if (strcmp(guess, hash) == 0)
    {
        retValue = 1;
    }

    return retValue;
}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
    FILE *f = fopen(filename, "r");
  
    int size = 50; // Array size
    char **pwds = (char **)malloc(size * sizeof(char *));
    char str[size];
    int i = 0;
    while (fscanf(f, "%s\n", str) != EOF)
    {
        if (i == size)  // The array is not big enough, increase the size
        {
            size = size + 50;
            char **newarr = (char **)realloc(pwds, size * sizeof(char *));
            if (newarr != NULL) pwds = newarr;
            else exit(1);
        }
        char *newstr = (char *)malloc((strlen(str) + 1) * sizeof(char));
        strcpy(newstr, str);
        pwds[i] = newstr;
        i++;
    }
    if (i == size) // The array is not big enough for a nulled ending, increase the size
    {
        size = size + 1;
        char **newarr = (char **)realloc(pwds, size * sizeof(char *));
        if (newarr != NULL) pwds = newarr;
        else exit(1);
    }
    pwds[i] = NULL; //set the last element of the array to null so we can look through them properly
 
    fclose(f);
    return pwds;
}

// takes an array of passwords, hashes them, 
// and returns a new array of hashed passwords
char **hashedDict(char **dict)
{
    int size = 50;
    char **newDict = (char**)malloc(size * sizeof(char *));
    int i = 0;
    // While there is still more stuff in the array, hash a password, and put it into the new array
    while (dict[i] != NULL) 
    {
        if (i == size) // Increases the size of the array if it isn't big enough
        {
            size = size + 50;
            char **newarr = (char **)realloc(newDict, size * sizeof(char *));
            if (newarr != NULL) newDict = newarr;
            else exit(1);
        }
        char *dictHash = md5(dict[i], strlen(dict[i]));
        newDict[i] = dictHash;
        i++;
    }
    if (i == size)
    {
        size = size + 1;
        char **newarr = (char **)realloc(newDict, size * sizeof(char *));
        if (newarr != NULL) newDict = newarr;
        else exit(1);
    }
    newDict[i] = NULL;
    
    return newDict;
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
    // Read the hash file into an array of strings
    char **hashes = readfile(argv[1]);
    
    // Read the dictionary file into an array of strings
    char **dict = readfile(argv[2]);
    
    
    char **hashdict = hashedDict(dict);
    
    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    int hashCount = 0;
    int dictCount = 0;
    while (hashes[hashCount] != NULL)
    {
        while (hashdict[dictCount] != NULL)
        {
            if (tryguess(hashes[hashCount], hashdict[dictCount]) == 1)
            {
                printf("%s\t%s\n", hashes[hashCount], dict[dictCount]);
                break;
            }
            dictCount++;
        }
        dictCount = 0;
        hashCount++;
    }
    
    // Free all malloced memory
    // Need to free each pointer inside our pointer to pointers
    // need two loops
    for (int i = 0; i < hashCount; i++)
    {
        free(hashes[i]);
    }
    free(hashes);
    
    int freeCount = 0;
    while (dict[freeCount] != NULL)
    {
        free(dict[freeCount]);
        free(hashdict[freeCount]);
        freeCount++;
    }
    free(dict);
    free(hashdict);
    
}
